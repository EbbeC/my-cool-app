import React, { Component } from 'react';

/**/
class Testing extends Component {
	constructor(props) {
		super(props);
		this.state = {
			seconds: 0
		};
	}

	componentDidMount() {
		this.timer = setInterval(() => this.setState({ seconds: this.state.seconds + 1 }), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.timer);
	}

	render() {
		return (
			<div>
				<h1>Yoyo! Welcome {this.props.name}</h1>
				<p>Waiting for {this.state.seconds} seconds for you to do something...</p>
			</div>
		);
	}
}
/** /

function Testing(props) {
	return <h1>Yoyo! Welcome {props.name}</h1>; 
}
/**/
export default Testing;