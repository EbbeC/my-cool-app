import React from 'react';
import SingleBet from './SingleBet';

const BetOptions = (props) => {
	console.log(props);
	return (
		<div className={props.classes}>
			<h2>Betting options</h2>
		{
			props.betOptions.length
			?
				props.betOptions.map((bet, index) => {

					switch ( bet.betOfferType.englishName ) {

						case 'Match': 
						case 'Over/Under': 
							return <SingleBet
								key={index} 
								bet={bet} 
								button={true} 
								dropdown={false} 
								match={props.match}
							/>
							// break;
						default:

							break;

					}

					return (
						<div key={index}>
							<p>{bet.criterion.label}</p>
							{
								bet.outcomes
								?
									<ul>
									{
										bet.outcomes.map((outcome, index) => {
											return (
												<li key={index}>{outcome.label}</li>
											);
										})
									}
									</ul>
								:
									<p>
									{
										() => {
											console.warn('bet');
											return <li>boop</li>
										}
									}
									</p>
							}
						</div>
					);
				})
			:
				null
		}
		</div>
	)
}

export default BetOptions;