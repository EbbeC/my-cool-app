import React from 'react';

const SingleBet = (props) => {

	// console.log(props);
	
	this.row = [];

	if ( props.button ) {
		props.bet.outcomes.map((outcome, index) => { // eslint-disable-line
			this.row.push(
				<div key={index} className="col-3 text-center">
					<button className="btn btn-primary">{outcome.label}</button>
				</div>
			);
		});
	}

	return (
		<div>
			<h3>{props.bet.criterion.label}</h3>
			<div className="row">
				{ this.row }
			</div>
		</div>
	)
}

export default SingleBet;