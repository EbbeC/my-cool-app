import React from 'react';

const Leagues = (props) => {
	return (
		<div className={props.classes}>
			<h2>League</h2>
		{
			props.leagues.groups 
			?
				props.leagues.groups.map((group, index) => {
					return (
						<div key={index} className={`lea-${index} ${(index === props.activeItem) ? 'active' : ''}`}>
							<a href="#league" onClick={ (e) => props.openMatches(e, group.termKey, index) }>{group.name}</a>
						</div>
					);
				})
			:
				null
		}
		</div>
	)
}

export default Leagues;