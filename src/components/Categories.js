import React from 'react';

const Categories = (props) => {
	return (
		<div className={props.classes}>
			<h2>Sport</h2>
		{ 
			props.categories.map((group, index) => {
				return (
					<div key={index} className={`cat-${index} ${(index === props.activeItem) ? 'active' : ''}`}>
						<a href="#category" onClick={ (e) => props.openLeagues(e, group.term, index) }>{group.name}</a>
					</div>
				);
			})
		}
		</div>
	)
}

export default Categories;