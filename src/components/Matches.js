import React from 'react';

const Matches = (props) => {
	return (
		<div className={props.classes}>
			<h2>Match</h2>
		{
			props.matches.map((bet, index) => {
				return (
					<div key={index} className={`mat-${index} ${(index === props.activeItem) ? 'active' : ''}`}>
						<a href="#match" onClick={ (e) => props.openBets(e, bet.event.id, index) }>{bet.event.name}</a>
					</div>
				);
			})
		}
		</div>
	)
}

export default Matches;