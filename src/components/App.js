import React, { Component } from 'react';
import '../style/App.css';
import Categories from './Categories';
import Matches from './Matches';
import Leagues from './Leagues';
import BetOptions from './BetOptions';
// import Testing from './testing';
import io from 'socket.io-client';

class App extends Component {
	
	constructor(props) {
		// Warming up
		super(props);
		this.state = {
			popular: [],
			groups: [],
			leagues: [],
			matches: [], // This actually gets some short bet list 
			allbets: [], // So let's get ALL the bet options here
			breadCrumb: '',
			activeItems: {
				group: -1,
				league: -1,
				match: -1
			}
		};
		// // Bind this to everything
		this.openLeagues = this.openLeagues.bind(this);
		this.openMatches = this.openMatches.bind(this);
		this.openBets = this.openBets.bind(this);
	}

	componentDidMount() {
		// Get that socket going
		this.socket = io('http://localhost:4000');
		
		// Popular
		if ( !this.state.popular.length ) {
			this.socket.emit('get popular', null);
		}
		this.socket.on('popular list', (games) => {
			let temp = [];
			games.map((game) => { // eslint-disable-line
				if ( game.name === 'popular' ) {
					game.events.map((event) => { // eslint-disable-line
						temp.push(event);
					});
				}
			});
			this.setState({ popular: temp });
		});

		// Groups
		if ( !this.state.groups.length ) {
			this.socket.emit('get groups', null);
		}
		this.socket.on('group list', (groups) => {
			this.setState({ groups: groups });
			// console.log(this.state.groups);
		});

		// Secondary groups
		this.socket.on('secondarygroup list', (groups) => {
			this.setState({ leagues: groups });
			// console.log(this.state.leagues.groups);
		});
		// Matches list
		this.socket.on('bets list', (bets) => {
			this.setState({ matches: bets }, () => {
				document.dispatchEvent(new Event('monkey'));
			});
			// console.log(this.state.matches);
		});
		// All bets for single event
		this.socket.on('allbets list', (bets) => {
			this.setState({ allbets: bets });
			// console.log(this.state.allbets);
		});
	}

	componentWillUnmount() {
		
	}

	openLeagues(e, term, index) {
		e.preventDefault();
		let temp = this.state.activeItems;
		temp.group = index;
		temp.league = -1;
		this.setState({ breadCrumb: term, allbets: [], matches: [], activeItems: temp });
		this.socket.emit('get secondarygroup', term);
	}

	openMatches(e, term, index) {
		e.preventDefault();
		let temp = this.state.activeItems;
		temp.league = index;
		temp.match = -1;
		this.setState({ allbets: [], activeItems: temp });
		this.socket.emit('get matches', this.state.breadCrumb + '/' + term);
	}

	openBets(e, eventId, index) {
		e.preventDefault();
		let temp = this.state.activeItems;
		temp.match = index;
		this.setState({ activeItems: temp });
		this.socket.emit('get allbets', eventId);
	}

	render() {
		return (
			<div className="App container-fluid" style={{height: window.innerHeight + 'px'}}>
				<div className="row h-100">

					<Categories 
						classes="column col-2" 
						categories={this.state.groups} 
						activeItem={this.state.activeItems.group} 
						openLeagues={this.openLeagues}
					/>

					<Leagues 
						classes={`column dropcolumn col-2 ${this.state.leagues.groups ? 'boop' : ''}`} 
						leagues={this.state.leagues} 
						openMatches={this.openMatches} 
						activeItem={this.state.activeItems.league}
					/>

					<Matches 
						classes={`column dropcolumn col-4 ${this.state.matches.length ? 'boop' : ''}`} 
						matches={this.state.matches} 
						openBets={this.openBets} 
						activeItem={this.state.activeItems.match}
					/>

					<BetOptions 
						classes={`column dropcolumn col-4 ${this.state.allbets.length ? 'boop' : ''}`} 
						betOptions={this.state.allbets} 
						match={this.state.matches[this.state.activeItems.match]} 
					/>

				</div>
			</div>
		);
	}
}

export default App;
